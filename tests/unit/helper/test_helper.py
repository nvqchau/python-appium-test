#!/usr/bin/env python3

import json
from typing import TYPE_CHECKING, Any, Dict

# import httpretty

from appium import webdriver
from appium.options.android import UiAutomator2Options
from appium.options.ios import XCUITestOptions
from tests.helpers.constants import SERVER_URL_BASE

if TYPE_CHECKING:
    # from httpretty.core import HTTPrettyRequestEmpty

    from appium.webdriver.webdriver import WebDriver


def appium_command(command: str) -> str:
    """Return a command of Appium

    :param command:
    :return:
        A string of command URL
    """
    return (
        f'{SERVER_URL_BASE}{command}'
        if SERVER_URL_BASE.endswith('/') or command.startswith('/')
        else f'{SERVER_URL_BASE}/{command}'
    )


def android_w3c_driver() -> 'WebDriver':
    """Return a W3C driver which is generated by a mock response for Android

    :return:
        `webdriver.webdriver.WebDriver`: An instance of WebDriver
    """

    response_body_json = json.dumps(
        {
            'sessionId': '1234567890',
            'capabilities': {
                'platform': 'LINUX',
                'desired': {
                    'platformName': 'Android',
                    'automationName': 'uiautomator2',
                    'platformVersion': '7.1.1',
                    'deviceName': 'Android Emulator',
                    'app': '/tests/apps/ApiDemos-debug.apk',
                },
                'platformName': 'Android',
                'automationName': 'uiautomator2',
                'platformVersion': '7.1.1',
                'deviceName': 'emulator-5554',
                'app': '/tests/apps/ApiDemos-debug.apk',
                'deviceUDID': 'emulator-5554',
                'appPackage': 'io.appium.android.apis',
                'appWaitPackage': 'io.appium.android.apis',
                'appActivity': 'io.appium.android.apis.ApiDemos',
                'appWaitActivity': 'io.appium.android.apis.ApiDemos',
            },
        }
    )

    httpretty.register_uri(httpretty.POST, appium_command('/session'), body=response_body_json)

    desired_caps = {
        'platformName': 'Android',
        'deviceName': 'Android Emulator',
        'app': 'path/to/app',
        'automationName': 'UIAutomator2',
    }

    options = UiAutomator2Options().load_capabilities(desired_caps)

    driver = webdriver.Remote(SERVER_URL_BASE, options=options)
    return driver


def ios_w3c_driver() -> 'WebDriver':
    """Return a W3C driver which is generated by a mock response for iOS

    :return:
        `webdriver.webdriver.WebDriver`: An instance of WebDriver
    """
    response_body_json = json.dumps(
        {
            'sessionId': '1234567890',
            'capabilities': {
                'device': 'iphone',
                'browserName': 'UICatalog',
                'sdkVersion': '11.4',
                'CFBundleIdentifier': 'com.example.apple-samplecode.UICatalog',
            },
        }
    )

    httpretty.register_uri(httpretty.POST, appium_command('/session'), body=response_body_json)

    desired_caps = {
        'platformName': 'iOS',
        'deviceName': 'iPhone Simulator',
        'app': 'path/to/app',
        'automationName': 'XCUITest',
    }

    options = XCUITestOptions().load_capabilities(desired_caps)

    driver = webdriver.Remote(SERVER_URL_BASE, options=options)
    return driver


def ios_w3c_driver_with_extensions(extensions: list['WebDriver'] | None = None) -> 'WebDriver':
    """Return a W3C driver which is generated by a mock response for iOS

    :param extensions: list of WebDriver
    :return:
        `webdriver.webdriver.WebDriver`: An instance of WebDriver
    """

    response_body_json = json.dumps(
        {
            'sessionId': '1234567890',
            'capabilities': {
                'device': 'iphone',
                'browserName': 'UICatalog',
                'sdkVersion': '11.4',
                'CFBundleIdentifier': 'com.example.apple-samplecode.UICatalog',
            },
        }
    )

    httpretty.register_uri(httpretty.POST, appium_command('/session'), body=response_body_json)

    desired_caps = {
        'platformName': 'iOS',
        'deviceName': 'iPhone Simulator',
        'app': 'path/to/app',
        'automationName': 'XCUITest',
    }

    options = XCUITestOptions().load_capabilities(desired_caps)

    driver = webdriver.Remote(SERVER_URL_BASE, options=options, extensions=extensions)
    return driver


# def get_httpretty_request_body(request: 'HTTPrettyRequestEmpty') -> Dict[str, Any]:
#     """Returns utf-8 decoded request body
#
#     :param request:
#     :return:
#         `Dict[str, Any]`
#     """
#     return json.loads(request.body.decode('utf-8'))
