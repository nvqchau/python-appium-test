import os
import socket
import time
from time import sleep
from typing import TYPE_CHECKING, Any, Callable
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from tests.functional.android.helper.test_helper import SLEEPY_TIME

if TYPE_CHECKING:
    from appium.webdriver.webdriver import WebDriver
    from appium.webdriver.webelement import WebElement


class NoAvailablePortError(Exception):
    pass


def get_available_from_port_range(from_port: int, to_port: int) -> int:
    """Returns available local port number.

    :param from_port: The start port to search
    :param to_port: The end port to search

    :return:
        available local port number which are found first
    """

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    for port in range(from_port, to_port):
        try:
            if sock.connect_ex(('localhost', port)) != 0:
                return port
        finally:
            sock.close()

    raise NoAvailablePortError(f'No available port between {from_port} and {to_port}')


def is_ci() -> bool:
    """Returns if current execution is running on CI

    :return:
        `True` if current executions is on CI
    """
    return os.getenv('CI', 'false') == 'true'


def wait_for_condition(method: Callable, timeout_sec: float = SLEEPY_TIME, interval_sec: float = 1, **kwargs) -> Any:
    """Wait while `method` returns the built-in objects considered false

    https://docs.python.org/3/library/stdtypes.html#truth-value-testing

    :param method: The target method to be waited
    :param timeout_sec: The timeout to be waited (sec.)
    :param interval_sec: The interval for wait (sec.)

    :return:
        value which `method` returns

    :raise:
        ValueError: When interval isn't more than 0
    """
    if interval_sec < 0:
        raise ValueError('interval_sec needs to be not less than 0')

    started = time.time()
    while time.time() - started <= timeout_sec:
        result = method(**kwargs)
        if result:
            break
        sleep(interval_sec)
    return result


def wait_for_element(driver: 'WebDriver', locator: str, value: str, timeout_sec: float = SLEEPY_TIME,
                     method: Callable = EC.element_to_be_clickable) -> 'WebElement':
    """Wait until the element located

    :param driver: WebDriver instance
    :param locator: Locator like WebDriver, Mobile JSON Wire Protocol
            (e.g. `appium.webdriver.common.appiumby.AppiumBy.ACCESSIBILITY_ID`)
    :param value: Query value to locator
    :param timeout_sec: Maximum time to wait the element. If time is over, `TimeoutException` is thrown
    :param method: callable(WebDriver)

    :raise:
        `selenium.common.exceptions.TimeoutException`

    :return:
        The found WebElement
    """
    return WebDriverWait(driver, timeout_sec).until(method((locator, value)))
