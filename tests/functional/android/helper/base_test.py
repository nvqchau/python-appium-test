#!/usr/bin/env python3

import base64
import os
from appium import webdriver
from appium.options.android import UiAutomator2Options
from appium.webdriver.appium_connection import AppiumConnection

from tests.helpers.constants import SERVER_URL_BASE
from tests.functional.android.helper import desired_capabilities
from tests.functional.android.helper.test_helper import SLEEPY_TIME
from tests.functional.test_helper import is_ci


class BaseTestCase:
    def setup_method(self) -> None:
        caps = desired_capabilities.get_desired_capabilities('ApiDemos-debug.apk.zip')
        options = UiAutomator2Options().load_capabilities(caps)

        self.driver = webdriver.Remote(SERVER_URL_BASE, options=options)
        self.driver.implicitly_wait(SLEEPY_TIME)

        if is_ci():
            self.driver.start_recording_screen()

    def teardown_method(self, method) -> None:
        if self.driver is not None:
            if is_ci():
                payload = self.driver.stop_recording_screen()
                video_path = os.path.join(os.getcwd(), method.__name__ + '.mp4')
                with open(video_path, 'wb') as fd:
                    fd.write(base64.b64decode(payload))

            self.driver.quit()

    def start_activity(self, package: str = None, activity: str = None, wait_activity_timeout: int = SLEEPY_TIME):
        self.driver.execute_script(
            'mobile: startActivity',
            {
                'component': f'{package}/{activity}',
            },
        )
        if wait_activity_timeout > 0:
            if not self.driver.wait_activity(activity, timeout=wait_activity_timeout):
                raise Exception(f'Cannot start activity: {activity}')
