#!/usr/bin/env python3

import os
from typing import Any, Dict, Optional


# Returns abs path relative to this file and not cwd
def PATH(p: str) -> str:
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '..', p))


def get_desired_capabilities(app: Optional[str] = None) -> Dict[str, Any]:
    desired_caps: Dict[str, Any] = {
        'platformName': 'Android',
        'deviceName': 'Android Emulator',
        'newCommandTimeout': 240,
        'automationName': 'UIAutomator2',
        'uiautomator2ServerInstallTimeout': 300000,
        'adbExecTimeout': 300000,
        'fullReset': True,
    }

    if app is not None:
        desired_caps['app'] = PATH(os.path.join('../..', 'apps', app))

    return desired_caps
