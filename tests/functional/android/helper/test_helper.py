#!/usr/bin/env python3

# the emulator is sometimes slow and needs time to think
SLEEPY_TIME = 60

# The package name of ApiDemos-debug.apk.zip
API_DEMO_PKG_NAME = 'io.appium.android.apis'
