#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from appium.webdriver.common.appiumby import AppiumBy
from tests.functional.android.helper.base_test import BaseTestCase
from tests.functional.android.helper.test_helper import API_DEMO_PKG_NAME
from tests.functional.test_helper import wait_for_element


class TestWebElement(BaseTestCase):
    def test_element_location_in_view(self) -> None:
        el = wait_for_element(self.driver, AppiumBy.ACCESSIBILITY_ID, 'Content')
        loc = el.location_in_view
        assert loc['x'] is not None
        assert loc['y'] is not None

    def test_set_text(self) -> None:
        wait_for_element(
            self.driver,
            AppiumBy.ANDROID_UIAUTOMATOR,
            'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text("Views").instance(0));'
        ).click()

        wait_for_element(self.driver, AppiumBy.ACCESSIBILITY_ID, 'Controls').click()
        wait_for_element(self.driver, AppiumBy.ACCESSIBILITY_ID, '1. Light Theme').click()

        el = wait_for_element(self.driver, AppiumBy.CLASS_NAME, 'android.widget.EditText')
        el.send_keys('original text')
        el.clear()
        el.send_keys('new text')

        assert 'new text' == el.text

    def test_send_keys(self) -> None:
        for text in ['App', 'Activity', 'Custom Title']:
            wait_for_element(self.driver, AppiumBy.XPATH, f'//android.widget.TextView[@text=\'{text}\']').click()

        el = wait_for_element(self.driver, AppiumBy.ID, '{}:id/left_text_edit'.format(API_DEMO_PKG_NAME))
        el.send_keys(' text')

        assert 'Left is best text' == el.text
