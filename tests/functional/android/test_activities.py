#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tests.functional.android.helper.base_test import BaseTestCase
from tests.functional.android.helper.test_helper import API_DEMO_PKG_NAME


class TestActivities(BaseTestCase):
    def test_current_activity(self) -> None:
        activity = self.driver.current_activity
        assert '.ApiDemos' == activity

    def test_start_activity_this_app(self) -> None:
        if self.start_activity(package=API_DEMO_PKG_NAME, activity='.ApiDemos'):
            self._assert_activity_contains('Demos')

        if self.start_activity(package=API_DEMO_PKG_NAME, activity='.accessibility.AccessibilityNodeProviderActivity'):
            self._assert_activity_contains('Node')

    def test_start_activity_other_app(self) -> None:
        self.start_activity(package=API_DEMO_PKG_NAME, activity='.ApiDemos')
        self._assert_activity_contains('Demos')

        self.start_activity(package='com.google.android.deskclock', activity='com.android.deskclock.DeskClock')
        self._assert_activity_contains('Clock')

    def _assert_activity_contains(self, activity: str) -> None:
        current = self.driver.current_activity
        assert activity in current
