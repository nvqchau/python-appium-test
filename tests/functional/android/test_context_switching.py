#!/usr/bin/env python3

import pytest
from selenium.webdriver import ActionChains
from selenium.webdriver.common.actions import interaction
from selenium.webdriver.common.actions.action_builder import ActionBuilder
from selenium.webdriver.common.actions.pointer_input import PointerInput
from appium.common.exceptions import NoSuchContextException
from appium.webdriver.common.appiumby import AppiumBy
from tests.functional.android.helper.base_test import BaseTestCase
from tests.functional.test_helper import wait_for_element, wait_for_condition


class TestContextSwitching(BaseTestCase):
    def test_contexts_list(self) -> None:
        self._enter_webview()
        contexts = self.driver.contexts
        assert 2 == len(contexts)

    def test_move_to_correct_context(self) -> None:
        self._enter_webview()
        assert 'WEBVIEW_io.appium.android.apis' == self.driver.current_context

    def test_actually_in_webview(self) -> None:
        self._enter_webview()
        el = wait_for_element(self.driver, AppiumBy.XPATH, '//a[@id="i am a link"]')
        assert el is not None

    def test_move_back_to_native_context(self) -> None:
        self._enter_webview()
        self.driver.switch_to.context(None)
        assert 'NATIVE_APP' == self.driver.current_context

    def test_set_invalid_context(self) -> None:
        with pytest.raises(NoSuchContextException):
            self.driver.switch_to.context('invalid name')

    def _enter_webview(self) -> None:
        btn = wait_for_element(self.driver, AppiumBy.ACCESSIBILITY_ID, 'Views')
        btn.click()
        self._scroll_to_webview()
        self._scroll_to_webview()
        current_context_len = len(self.driver.contexts)
        btn_web_view = wait_for_element(self.driver, AppiumBy.ACCESSIBILITY_ID, 'WebView')
        btn_web_view.click()

        wait_for_condition(lambda driver: len(driver.contexts) > current_context_len, driver=self.driver)
        self.driver.switch_to.context(self.driver.contexts[-1])

    def _scroll_to_webview(self) -> None:
        actions = ActionChains(self.driver)
        actions.w3c_actions = ActionBuilder(self.driver, mouse=PointerInput(interaction.POINTER_TOUCH, "touch"))
        actions.w3c_actions.pointer_action.move_to_location(393, 1915)
        actions.w3c_actions.pointer_action.pointer_down()
        actions.w3c_actions.pointer_action.move_to_location(462, 355)
        actions.w3c_actions.pointer_action.release()
        actions.perform()
