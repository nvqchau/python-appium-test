#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from appium.webdriver.applicationstate import ApplicationState
from tests.functional.android.helper.base_test import BaseTestCase
from tests.functional.android.helper.desired_capabilities import PATH
from tests.functional.android.helper.test_helper import API_DEMO_PKG_NAME


class TestApplications(BaseTestCase):
    def test_background_app(self) -> None:
        self.driver.background_app(1)

    def test_is_app_installed(self) -> None:
        assert not self.driver.is_app_installed('qwertyuiop')
        assert self.driver.is_app_installed(API_DEMO_PKG_NAME)

    def test_install_app(self) -> None:
        self.driver.remove_app(API_DEMO_PKG_NAME)
        assert not self.driver.is_app_installed(API_DEMO_PKG_NAME)
        self.driver.install_app(PATH(os.path.join('..', '..', 'apps', 'ApiDemos-debug.apk.zip')))
        assert self.driver.is_app_installed(API_DEMO_PKG_NAME)

    def test_remove_app(self) -> None:
        assert self.driver.is_app_installed(API_DEMO_PKG_NAME)
        self.driver.remove_app(API_DEMO_PKG_NAME)
        assert not self.driver.is_app_installed(API_DEMO_PKG_NAME)

    def test_app_management(self) -> None:
        app_id = self.driver.current_package
        assert self.driver.query_app_state(app_id) == ApplicationState.RUNNING_IN_FOREGROUND
        self.driver.background_app(-1)
        assert self.driver.query_app_state(app_id) < ApplicationState.RUNNING_IN_FOREGROUND
        self.driver.activate_app(app_id)
        assert self.driver.query_app_state(app_id) == ApplicationState.RUNNING_IN_FOREGROUND

    def test_app_strings(self) -> None:
        strings = self.driver.app_strings()
        assert u'You can\'t wipe my data, you are a monkey!' == strings[u'monkey_wipe_data']

    def test_app_strings_with_language(self) -> None:
        strings = self.driver.app_strings('en')
        assert u'You can\'t wipe my data, you are a monkey!' == strings[u'monkey_wipe_data']

    def test_app_strings_with_language_and_file(self) -> None:
        strings = self.driver.app_strings('en', 'some_file')
        assert u'You can\'t wipe my data, you are a monkey!' == strings[u'monkey_wipe_data']
