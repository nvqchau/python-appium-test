from appium.webdriver.common.appiumby import AppiumBy

appPackage = {
    'fake_location': 'com.lerist.fakelocation',
    'chrome': "com.android.chrome",
    'settings': 'com.android.settings',
    'gmail': 'com.google.android.gm',
}
appActivity = {
    'fake_location': '.ui.activity.MainActivity',
    'chrome': "com.google.android.apps.chrome.Main",
    'settings': '.Settings',
    'gmail': '.ConversationListActivityGmail',
    'gmail_wel': '.welcome.WelcomeTourActivity',
}

appBundleId = {
    'chrome': 'com.google.chrome.ios',
    'gmail': 'com.google.Gmail',
    'safari': 'com.apple.mobilesafari',
    'settings': 'com.apple.Preferences',
    'appstore': 'com.apple.AppStore',
}

android_base = {
    'text': lambda value: (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("{0}")'.format(value)),
    'text_contains': lambda value: (
        AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().textContains("{0}")'.format(value)),
    'text_view': lambda value: (AppiumBy.XPATH, '//android.widget.TextView[@text="{0}"]'.format(value)),
    'button': lambda value: (
        AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().className("android.widget.Button").text("{0}")'.format(value)),
    'edittext': lambda value: (AppiumBy.XPATH, '//android.widget.EditText[@text="{0}"]'.format(value)),
    'desc_contains': lambda value: (
        AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().descriptionContains("{0}")'.format(value)),  # content-desc属性
}

ios_base = {
    'value': lambda v: (AppiumBy.XPATH, '//*[@value="{}"]'.format(v)),
    'value_contains': lambda v: (AppiumBy.XPATH, '//*[contains(@value,"{}")]'.format(v)),
    'name': lambda v: (AppiumBy.XPATH, '//*[@name="{}"]'.format(v)),
    'name_contains': lambda v: (AppiumBy.XPATH, '//*[contains(@name,"{}")]'.format(v)),
    'btn_name': lambda v: (AppiumBy.IOS_PREDICATE, 'type=="XCUIElementTypeButton" AND name=="{}"'.format(v)),
    'btn_name_contains': lambda v: (
        AppiumBy.IOS_PREDICATE, 'type=="XCUIElementTypeButton" AND name CONTAINS "{}"'.format(v)),
    'switch_name': lambda v: (AppiumBy.IOS_PREDICATE, 'type=="XCUIElementTypeSwitch" AND name=="{}"'.format(v)),
    'switch_name_contains': lambda v: (
        AppiumBy.IOS_PREDICATE, 'type=="XCUIElementTypeSwitch" AND name CONTAINS "{}"'.format(v)),
    'cell_name': lambda v: (AppiumBy.IOS_PREDICATE, 'type=="XCUIElementTypeCell" AND name=="{}"'.format(v)),
    'cell_name_contains': lambda v: (
        AppiumBy.IOS_PREDICATE, 'type=="XCUIElementTypeCell" AND name CONTAINS "{}"'.format(v)),
}

'''
Keys on the iOS keyboard, case sensitive.
Special keys shift, delete, more, space, @, ., Return, Next keyboard (switch text, such as Chinese and English)
are not case sensitive
'''
ios_keyboard = {
    'done': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label MATCHES "Done|完成"`]'),
    'key': lambda k: (AppiumBy.ACCESSIBILITY_ID, '{0}'.format(k))
}


def scrollable(locators: [list, str]):
    if isinstance(locators, list):
        return (AppiumBy.ANDROID_UIAUTOMATOR,
                'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().%s)' % '.'.join(
                    locators))
    elif isinstance(locators, str):
        return (AppiumBy.ANDROID_UIAUTOMATOR,
                'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().%s)' % locators)


def selector(locators: [list, str]):
    if isinstance(locators, list):
        return AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().%s' % '.'.join(locators)
    elif isinstance(locators, str):
        return AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().%s' % locators


settings = {
    'nothing_en': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("%s")' % 'Nothing'),
    'nothing_zh': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("%s")' % '无'),
    'None': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("%s")' % 'None'),
    'fake_location': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("{0}")'.format('Fake Location')),
    'select_mock_app_en': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().textContains("%s")' % 'Mock'),
    'select_mock_app_zh': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().textContains("%s")' % '模拟位置'),
    'select_text_zh': '选择模拟位置信息应用',
    'select_text_en': 'Select mock location app',

    # ********* iOS ************
    'ios_setting_page_title': (AppiumBy.XPATH, '//XCUIElementTypeStaticText[@name="Settings"]'),
    'ios_setting_search_field': (AppiumBy.ACCESSIBILITY_ID, 'Search'),
    # Bluetooth page element: When turned on, the value attribute is 1, and when turned off, the value attribute is 0
    'ios_bluetooth': (
        AppiumBy.IOS_PREDICATE, 'type=="{0}" AND name=="{1}"'.format('XCUIElementTypeSwitch', 'Bluetooth')),
    'ios_bluetooth2': (AppiumBy.XPATH, '//XCUIElementTypeSwitch[@name="Bluetooth"]'),
    # `setting` home page element: On/Off element, click to enter the Bluetooth page
    'ios_bluetooth_item': (
        AppiumBy.XPATH,
        '//XCUIElementTypeStaticText[@name="Bluetooth"]/following-sibling::XCUIElementTypeStaticText[1]'),
    'ios_setting_items': lambda x: (AppiumBy.IOS_PREDICATE, 'type=="XCUIElementTypeCell" AND name=="{0}"'.format(x)),
    'ios_setting_toggles': lambda x: (AppiumBy.XPATH, '//XCUIElementTypeSwitch[@name="{0}"]'.format(x)),
    'ios_setting_wifi': (AppiumBy.XPATH, '//XCUIElementTypeSwitch[@name="Wi-Fi"]'),
    'ios_back_to_current_app': (AppiumBy.ACCESSIBILITY_ID, 'breadcrumb'),  # Remember to add `approach` to this = 'p'
    'ios_setting_items_title': lambda x: (
        AppiumBy.IOS_PREDICATE, 'type=="XCUIElementTypeOther" AND name=="{0}"'.format(x)),
    'ios_general': (AppiumBy.IOS_PREDICATE, 'type=="{0}" AND name=="{1}"'.format('XCUIElementTypeCell', '通用')),
    'ios_date&time': (
        AppiumBy.IOS_PREDICATE, 'type=="{0}" AND name=="{1}"'.format('XCUIElementTypeCell', 'Date & Time')),
    'ios_profile&devicemanagement': (
        AppiumBy.IOS_PREDICATE,
        'type=="{0}" AND name CONTAINS "{1}"'.format('XCUIElementTypeCell', 'Device Management')),
    'ios_trust_app_btn': lambda x: (
        AppiumBy.IOS_PREDICATE, 'type=="XCUIElementTypeStaticText" AND value=="Trust “{0}”"'.format(x)),
    # e.g. Trust “Fisher-Price, Inc.”
    'ios_trust_app_dialog_title': lambda x: (AppiumBy.IOS_PREDICATE,
                                             'type=="XCUIElementTypeStaticText" AND value CONTAINS "Trust “iPhone Distribution: {0}”"'.format(
                                                 x)),
    # e.g. Trust “iPhone Distribution: Fisher-Price, Inc.” Apps on This iPhone
    'ios_trust_btn': (AppiumBy.ACCESSIBILITY_ID, 'Trust'),
    # A 24-hour button. If the current 12-hour format is 12-hour, its value is 0, otherwise it is 1.
    'ios_24hr': (AppiumBy.IOS_PREDICATE, 'type=="{0}" AND name=="{1}"'.format('XCUIElementTypeCell', '24-Hour Time')),
    'ios_24hr_x': (AppiumBy.XPATH, '//XCUIElementTypeCell[@name="24-Hour Time"]'),
}

app_store = {
    'continue': (AppiumBy.IOS_PREDICATE, 'label == "继续" AND name == "继续" AND type == "XCUIElementTypeButton"'),
    # Continue to pop-up page
    'allow_when_using': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "使用App时允许"`]'),
    'app_item': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "App"`]'),
    'account': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "我的帐户"`]'),
    'search_item': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "搜索"`]'),
    'search_field': (
        AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeNavigationBar[`name == "搜索"`]/XCUIElementTypeSearchField'),
    # Appears after clicking `searchfield`
    'keyboard_continue': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "继续"`]'),
    'keyboard_search_btn': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`name == "Search"`]'),
    # Appears after clicking `searchfield`
    'progress_circle': (
        AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeActivityIndicator[`label MATCHES "正在载入|进行中"`]'),
    # Button to load search results
    'retry': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "重试"`]'),  # 搜索失败时
    'app': lambda a: (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label CONTAINS "{}"`]'.format(a)),
    # Fisher-Price® Smart Connect™
    # `app` `details page
    'navigate_search_btn': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeNavigationBar[`name == "搜索"`]`]'),
    # `app` details page
    'reload_btn': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "重新下载"`]'),
    # `app` details page `first time download`
    'get_btn': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "获取"`]'),
    'upgrade_btn': (AppiumBy.IOS_CLASS_CHAIN, ''),
    # `circle` button
    'in_process': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "正在载入"`]'),
    # Pause button
    'downloading': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "正在下载"`]'),
    'open_app': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "打开"`]'),

}

notification = {
    'ios_notification': lambda msg_title: (AppiumBy.IOS_PREDICATE,
                                           'type=="XCUIElementTypeScrollView" AND (name CONTAINS "{0}" OR label CONTAINS "{0}")'.format(
                                               msg_title)),
    # When there are multiple notifications for a certain app, they usually overlap and need to be clicked to expand.
    # You need to pass in the name value, which is usually the app title of the expanded message, such as: SMART CONNECT
    'ios_nt_msg': lambda c: (AppiumBy.IOS_PREDICATE,
                             'type=="XCUIElementTypeButton" AND name=="NotificationCell" AND label CONTAINS "{}"'.format(
                                 c)),
    # (Swipe right to delete) Such as message: Animal projection on your My Child's Deluxe Soother is turning off soon.
    'ios_nt_clear': lambda msg_title: (AppiumBy.IOS_CLASS_CHAIN,
                                       '**/XCUIElementTypeButton[`label BEGINSWITH "{}"`]/XCUIElementTypeButton[`label == "Clear"`][1]'.format(
                                           msg_title)),  # 向左滑动消息出现的Clear button
    'ios_nt_clear_all': (AppiumBy.IOS_CLASS_CHAIN, '**/XCUIElementTypeButton[`label == "Clear All"`]'),
    # Swipe the `Clear All button` to the left where the message appears
    'ios_clear_all_btn': (
        AppiumBy.IOS_PREDICATE,
        'type=="XCUIElementTypeButton" AND name=="clear-button" AND value=="Notification Center"'),
    # Clear all app messages button (x)
    # Clear an app message button (x) without label value, such as Smart Connect
    'ios_clear_btn': lambda app: (AppiumBy.XPATH,
                                  '//XCUIElementTypeStaticText[@name="{}"]/../following::XCUIElementTypeButton[@name="clear-button"]'.format(
                                      app)),
    # Click the clear button that appears in x, universal
    'ios_confirm_clear': (
        AppiumBy.IOS_PREDICATE, 'type=="XCUIElementTypeButton" AND name=="clear-button" AND label=="Confirm Clear"'),
}

camera = {
    # App built-in camera interface
    'ios_capture': (AppiumBy.ACCESSIBILITY_ID, 'PhotoCapture'),
    'ios_cancel_capture': (AppiumBy.ACCESSIBILITY_ID, 'Cancel'),
    'ios_switch_camera': (AppiumBy.ACCESSIBILITY_ID, 'FrontBackFacingCameraChooser'),
    # The flash has three values, namely Automatic, On, and Off.
    # It cannot be set using send_keys.
    # You need to click on the Flash icon and then select it in the secondary menu below.
    'ios_flash_light': (AppiumBy.ACCESSIBILITY_ID, 'Flash'),
    'ios_flash_auto': (AppiumBy.ACCESSIBILITY_ID, 'Auto'),
    'ios_flash_on': (AppiumBy.ACCESSIBILITY_ID, 'On'),
    'ios_flash_off': (AppiumBy.ACCESSIBILITY_ID, 'Off'),
    # Preview interface
    'ios_retake': (AppiumBy.ACCESSIBILITY_ID, 'Retake'),
    'ios_use': (AppiumBy.ACCESSIBILITY_ID, 'Use Photo'),
    # Cut interface
    'ios_crop_use': (AppiumBy.ACCESSIBILITY_ID, 'useButton'),
    'ios_crop_cancel': (AppiumBy.ACCESSIBILITY_ID, 'cancelButton'),
}

albums = {
    # App photo album interface (select photo)
    'ios_cancel': (AppiumBy.ACCESSIBILITY_ID, 'Cancel'),
    # For each album, just quote the name of the album when using it.
    # The system default albums generally include:
    # Camera Roll, Recently Added, Screenshots, and the albums created by each app.
    'ios_albums': lambda x: (AppiumBy.ACCESSIBILITY_ID, '{0}'.format(x)),
    # The return key located in the upper left corner is used to return to the album list
    'ios_back_btn': (AppiumBy.ACCESSIBILITY_ID, 'Photos'),
    # For photos in the album, generally the latest photo will appear at the end of the album.
    # You can introduce last() to get it, or enter a number to get the Nth photo.
    # For example, if you enter 200, you will get the 200th photo in the album.
    # Here is what you need Note that the photos that can be displayed in the album are limited.
    # If you select a photo that is not displayed and click it for the first time,
    # the album will automatically jump to the display position of the selected photo.
    # Click it again to finally select this photo.
    'ios_photos_by_position': lambda x: (
        AppiumBy.XPATH, '//XCUIElementTypeCollectionView[@name="PhotosGridView"]/XCUIElementTypeCell[{0}]'.format(x)),

    # 剪切界面
    'ios_crop_use': (AppiumBy.ACCESSIBILITY_ID, 'useButton'),
    'ios_crop_cancel': (AppiumBy.ACCESSIBILITY_ID, 'cancelButton'),
}

fake_location = {
    'menu_btn': (AppiumBy.ACCESSIBILITY_ID, 'Open navigation drawer'),
    'start_to_fake': (AppiumBy.ID, 'com.lerist.fakelocation:id/f_fakeloc_tv_service_switch'),
    'add_btn': (AppiumBy.ID, 'com.lerist.fakelocation:id/fab'),
    'current_coords': (AppiumBy.ID, 'com.lerist.fakelocation:id/f_fakeloc_tv_current_latlong'),
    'running_mode': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("{0}")'.format('运行模式')),
    'no_root_mode': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().textContains("{0}")'.format('NOROOT')),
    'root_mode': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().textContains("{0}")'.format('ROOT（推荐）')),
    'permission_allow': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("{0}")'.format('允许')),
    'permission_allow_id': (AppiumBy.ID, 'com.android.packageinstaller:id/dialog_container'),
    'title_choose_location': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("{0}")'.format('选择位置')),
    'search_btn': (AppiumBy.ID, 'com.lerist.fakelocation:id/m_item_search'),
    'search_box': (AppiumBy.ID, 'com.lerist.fakelocation:id/l_search_panel_et_input'),
    'confirm_btn': (AppiumBy.ID, 'com.lerist.fakelocation:id/a_map_btn_done'),
    'back_btn': (AppiumBy.ACCESSIBILITY_ID, '转到上一层级'),
    'update_next_time': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("{0}")'.format('下次再说')),
    'forward_toset': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("{0}")'.format('前往设置')),
    'get_permission': (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("{0}")'.format('前往授权')),

    # Common elements-prompt box & prompt box confirmation and cancel button
    'native_dialog': (AppiumBy.ID, 'android:id/parentPanel'),
    'prompt_dialog': (AppiumBy.ID, 'com.lerist.fakelocation:id/parentPanel'),
    'dialog_confirm_btn': (AppiumBy.ID, 'android:id/button1'),
    'dialog_cancel_btn': (AppiumBy.ID, 'android:id/button2'),
}
