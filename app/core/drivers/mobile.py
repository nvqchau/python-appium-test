#!/usr/bin/env python3
# encoding: utf-8

import time
from appium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.actions import interaction
from selenium.webdriver.common.actions.action_builder import ActionBuilder
from selenium.webdriver.common.actions.pointer_input import PointerInput
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from app.core.drivers.client import Client
from app.core.drivers import locate_method


class Mobile(Client):

    def __init__(self, driver=None, device_info=None, remote=False):
        super().__init__(device_info)
        if driver:
            self.driver = driver
        self.remote = remote
        self.locate_method = locate_method
        self.to_find = {
            'up': self.scroll_up,
            'down': self.scroll_down,
            'left': self.swipe_left,
            'right': self.swipe_right,
        }
        self.udid = self.device_info.get('caps').get('udid') if self.device_info else None
        self.host = self.device_info.get('host') if self.device_info else None
        self.cond = ec.visibility_of_element_located

    def set_caps(self, **kwargs):
        self.desired_caps.update(kwargs)
        return self

    def start(self, host=None, **kwargs):
        url = host or self.host
        self.desired_caps.update(kwargs)
        self.driver = webdriver.Remote(url, self.desired_caps)
        return self.driver

    def scroll_to_find(self, by, locator, condition=None, timeout=3, interval=0.2,
                       direction='down', interrupt=None, **kwargs):
        """
        Scroll up/down/left/right to find an element
        direction: up, down
        """
        latter_source = self.driver.page_source
        while not self.find(by, locator, condition, timeout=timeout):
            if interrupt:
                if self.find(*interrupt):
                    return
            former_source = latter_source
            self.to_find[direction](**kwargs)
            time.sleep(interval)
            latter_source = self.driver.page_source
            if former_source == latter_source:
                return
        return self.Element

    def switch_to_native(self):
        self.driver.switch_to.context(self.driver.contexts[0])

    def switch_to_webview(self):
        self.driver.switch_to.context(self.driver.contexts[-1])

    @staticmethod
    def convert_symbol(text: str):
        txt = ""
        for t in text:
            if t == "'":
                txt = txt + "\"'\""
            elif t == '"':
                txt = txt + '\'"\''
            elif t == "`":
                txt = txt + "\'`\'"
            elif t == "(":
                txt = txt + "\'(\'"
            elif t == ")":
                txt = txt + "\')\'"
            elif t == "|":
                txt = txt + "\'|\'"
            elif t == "<":
                txt = txt + "\'<\'"
            elif t == ">":
                txt = txt + "\'>\'"
            elif t == "&":
                txt = txt + '\'&\''
            elif t == ";":
                txt = txt + '\';\''
            else:
                txt = txt + t
        return txt

    def input_text(self, text: [int, float, str], by: str, locator: str):
        """
        Enter text in the specified element
        :param text: Enter text
        :param by: element positioning method
        :param locator: Element parameters, such as name, id, etc.
        :return:
        """
        try:
            text = self.convert_symbol(str(text))
            element = WebDriverWait(self.driver, 20, 0.5).until(ec.element_to_be_clickable(
                (by, locator)))
            element.clear()
            self.driver.set_value(element, text)
        except TimeoutException:
            raise Exception(f"Element not found: {by} {locator}")

    def scroll(self, direction, start_pos=None, end_pos=None, duration: int = 400, rect=None):
        _scroll = {
            'up': (0.3, 0.8), 'down': (0.8, 0.3),
            'left': (0.1, 0.9), 'right': (0.9, 0.1),
        }
        if rect:
            pass

    def scroll_down(self, start_pos: float = 0.8, end_pos: float = 0.3, duration: int = 800, element=None):
        """
        Simulates a finger sliding up from the bottom of the screen (the page scrolls up to display the content below)
        """
        if element is None:
            scr_width = self.driver.get_window_size()['width']
            scr_height = self.driver.get_window_size()['height']
            self.driver.swipe(scr_width / 2, scr_height * start_pos, scr_width / 2, scr_height * end_pos,
                              duration=duration)
        else:
            rect = element.rect
            self.driver.swipe(rect['x'] + rect['width'] / 2, rect['y'] + rect['height'] * start_pos,
                              rect['x'] + rect['width'] / 2, rect['y'] + rect['height'] * end_pos,
                              duration=duration)

            # self.driver.execute_script("mobile: swipe", {"direction": "up"})

    def scroll_up(self, start_pos: float = 0.3, end_pos: float = 0.8, duration: int = 800, element=None):
        """
        Simulate a finger sliding down from the top of the screen (the page scrolls down to display the content above)

        :param start_pos: Starting position as a percentage of the screen or original width
        :param end_pos: End position as a percentage of the screen or original width
        :param duration: Slide duration
        :param element: Whether to specify the element
        :return:
        """
        if element is None:
            scr_width = self.driver.get_window_size()['width']
            scr_height = self.driver.get_window_size()['height']
            self.driver.swipe(scr_width / 2, scr_height * start_pos, scr_width / 2, scr_height * end_pos,
                              duration=duration)
        else:
            rect = element.rect
            self.driver.swipe(rect['x'] + rect['width'] / 2, rect['y'] + rect['height'] * start_pos,
                              rect['x'] + rect['width'] / 2, rect['y'] + rect['height'] * end_pos,
                              duration=duration)
        # self.driver.execute_script("mobile: swipe", {"direction": "down"})

    def swipe_right(self, start_pos: float = 0.9, end_pos: float = 0.1, duration: int = 800, element=None):
        """
        Simulate the operation of sliding a finger from right to left
        (the page scrolls to the left to display the content on the right)

        :param start_pos: Starting point as a percentage of the screen or original width
        :param end_pos: End point as a percentage of the screen or original width
        :param duration: Slide duration
        :param element: Whether to specify the element
        :return: None
        """
        if element is None:
            width = self.driver.get_window_size()['width']
            height = self.driver.get_window_size()['height']
            self.driver.swipe(width * start_pos, height / 2, width * end_pos, height / 2, duration=duration)
        else:
            rect = element.rect
            self.driver.swipe(rect['x'] + rect['width'] * start_pos, rect['y'] + rect['height'] / 2,
                              rect['x'] + rect['width'] * end_pos, rect['y'] + rect['height'] / 2, duration=duration)

    def swipe_left(self, start_pos: float = 0.1, end_pos: float = 0.9, duration: int = 800, element=None):
        """
        Simulate the operation of sliding a finger from left to right
        (the page scrolls to the right to display the content on the left)

        :param start_pos: Starting point as a percentage of the screen or original width
        :param end_pos: End point as a percentage of the screen or original width
        :param duration: Slide duration
        :param element: Whether to specify the element
        :return: None
        """
        if element is None:
            width = self.driver.get_window_size()['width']
            height = self.driver.get_window_size()['height']
            self.driver.swipe(width * start_pos, height / 2, width * end_pos, height / 2, duration=duration)
        else:
            rect = element.rect
            self.driver.swipe(rect['x'] + rect['width'] * start_pos, rect['y'] + rect['height'] / 2,
                              rect['x'] + rect['width'] * end_pos, rect['y'] + rect['height'] / 2, duration=duration)

    def scroll_down_to_bottom(self, start_pos: float = 0.8, end_pos: float = 0.3, duration: int = 800, interval: float = 0.5, element=None):
        """Scroll to bottom of page

        :return:
        """
        try:
            latter_source = self.driver.page_source
            while True:
                former_source = latter_source
                self.scroll_down(start_pos, end_pos, duration, element)
                time.sleep(interval)
                latter_source = self.driver.page_source
                if former_source == latter_source:
                    break
        except Exception as msg:
            raise Exception(msg)

    def scroll_up_to_top(self, start_pos: float = 0.3, end_pos: float = 0.8, duration: int = 800, interval: float = 0.5,
                         element=None):
        """Scroll to top of page

        :return:
        """
        try:
            latter_source = self.driver.page_source
            while True:
                former_source = latter_source
                self.scroll_up(start_pos, end_pos, duration, element)
                time.sleep(interval)
                latter_source = self.driver.page_source
                if former_source == latter_source:
                    break
        except Exception as msg:
            raise Exception(msg)

    def touch(self, element):
        """
        Click on the element with by getting the midpoint coordinate of the element
        :param element:
        :return:
        """
        x = element.location['x'] + element.size['width'] / 2
        y = element.location['y'] + element.size['height'] / 2
        actions = ActionChains(self.driver)
        actions.w3c_actions = ActionBuilder(self.driver, mouse=PointerInput(interaction.POINTER_TOUCH, "touch"))
        actions.w3c_actions.pointer_action.move_to_location(x=x, y=y)
        actions.w3c_actions.pointer_action.pointer_down()
        actions.w3c_actions.pointer_action.release()
        actions.perform()

    def current_context(self):
        return self.driver.current_activity
